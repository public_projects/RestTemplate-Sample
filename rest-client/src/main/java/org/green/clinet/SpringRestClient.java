/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.green.clinet;

/**
 *
 * @author greenhorn
 */
import java.net.URISyntaxException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
 
@Component
public class SpringRestClient {
 
    private final RestTemplate restTemplate;
 
    public SpringRestClient() {
        ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();
        this.restTemplate = new RestTemplate(requestFactory);
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
    }
 
    public void sendRequest2() throws URISyntaxException {
        String requestJsonString = "{ \"name\": \"user1\"}";
 
        String requestUrl = "http://localhost:8080/employee";
 
        // set headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(requestJsonString, headers);
 
        // send request and parse result
        ResponseEntity<String> restResponse = restTemplate
                .exchange(requestUrl, HttpMethod.POST, entity, String.class);
 
        System.err.println("response string: " + restResponse.getBody());
    }
 
    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        int timeout = 60000;
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout)
                .setConnectionRequestTimeout(timeout)
                .setSocketTimeout(timeout)
                .build();
        CloseableHttpClient client = HttpClientBuilder
                .create()
                .setDefaultRequestConfig(config)
                .build();
        return new HttpComponentsClientHttpRequestFactory(client);
    }
}
