/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.green.controller;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author greenhorn
 */
@RestController
public class EmployeeController {
     
    @RequestMapping(path = "/employee")
    public @ResponseBody Map<String, String> getEmployee(@RequestBody JsonNode jsonNode){
        String name = jsonNode.get("name").asText();
        System.err.println("[Calling][getEmployee][name]"+name);
        Map<String, String> namefEmployee = new HashMap<>();
        namefEmployee.put("Name of Employee", name);
        return namefEmployee;
    }
}